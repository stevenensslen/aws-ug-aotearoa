import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/account",
    name: "account",
    component: () => import("../components/SignIn")
  },
  {
    path: "/signup",
    name: "signup",
    component: () => import("../components/SignUp")
  },
  {
    path: "/members-only",
    name: "members-only",
    component: () => import("../views/MembersOnly"),
    meta: { requiresAuth: true }
  },
  {
    path: "/palette",
    name: "palette",
    component: () => import("../components/Palette"),
    meta: { requiresAuth: true }
  },
  // {
  //   path: "/about",
  //   name: "about",
  //   component: () => import("../views/About")
  // },
  {
    path: "/code-of-conduct",
    name: "code-of-conduct",
    component: () => import("../views/SocietyRules")
  },
  { path: "*", component: () => import("../components/NotFoundComponent") }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  }
});

let status = true;

router.beforeResolve((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    Vue.prototype.$Amplify.Auth.currentAuthenticatedUser()
      .then(data => {
        if (data && data.signInUserSession) {
          if (status) status = data;
        }
        next();
        // eslint-disable-next-line prettier/prettier
      }).catch((e) => {
        next({
          path: "/account"
        });
        status = e;
      });
  }
  next();
});

export default router;
